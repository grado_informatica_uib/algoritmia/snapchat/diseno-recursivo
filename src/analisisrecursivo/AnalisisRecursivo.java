/*
 *  

Heu d’aplicar l’anàlisi per al disseny d’algorismes recursius per proposar 
una solució al següent problema:

Es tenen dos arrays (T[] a, T[] b).  Es vol que l’algorisme calculi 
la mitja i la mediana en un ordre de complexitat O(n log n)

A més d’aplicar l’anàlisi per al disseny d’algorismes recursius, 
he de demostrar el seu ordre de complexitat i fer la transformació 
de recursiu a iteratiu, en ambdós casos explicant quines passes heu seguit.

Es podria reduir l’ordre de complexitat si els arrays estiguessin ordenats?
Quina seria l’ordre de complexitat i per què?

Criteris d'avaluació

Màxim 2 alumnes per projecte.

L'informe es puja com a taller a Aula Digital en format PDF, 
on hi ha una rúbrica per a l'autoavaluació i coavaluació.

Competències: CCM06 CTR01 CTR02 CTR07

Apartats de la rúbrica (sempre que la resposta sigui no s’ha 
de justificar en els comentaris):
La solució recursiva és correcte? (Raona la resposta).
La solució iterativa és correcte? (Raona la resposta).

S’han aplicat totes les passes de l’anàlisi per al disseny 
d’algorismes recursius? (Raona la resposta).
S’ha demostrat correctament el seu ordre de complexitat? (Raona la resposta).
La resposta a la pregunta del cas en què els arrays estiguessin ordenats és 
correcte? (Raona la resposta).
En general com valores el projecte? (Raona la resposta).
 */
package analisisrecursivo;

/**
 *
 * @author hmedcab
 */
public class AnalisisRecursivo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
